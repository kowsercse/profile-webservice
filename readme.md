Source code can be accessed from here: [https://bitbucket.org/kowsercse/profile-webservice](https://bitbucket.org/kowsercse/profile-webservice).
Application is deployed on Azure Cloud at [http://ipeertest.cloudapp.net:8080/profile-webservice/](http://ipeertest.cloudapp.net:8080/profile-webservice/).
Information is provided below on how to access it.

# Install
  1. Use `setup/db.sql` to create the `Profile` table. 
  1. Copy `setup/profile-webservice.properties` to __user's home directory__ using `cp setup/profile-webservice.properties ~`.
  1. Update `~/profile-webservice.properties` with database information.
  1. Execute `mvn clean install` to run unit test and build the war module.
  1. Execute `mvn tomcat7:run` to deploy the war using the embedded tomcat.
   
# Answer 1
Please check: `com.kowsercse.entity.Profile`.

# Answer 2
Please check: `com.kowsercse.service.parsers.profile.ProfileCsvFileParser`. Unit test is written for this class.

# Answer 3
Please check: `com.kowsercse.service.parsers.profile.ProfileCsvParser`. Unit test is written for this class.

# Answer 4
Please check: `com.kowsercse.service.ProfileColorGroupService`. Unit test is written for this class. Also unit test is 
used to initiate the print command. I believe it is okay.

# Answer 5
Please check: `com.kowsercse.webservice.ProfileResource`. Spring mvc is used for webservice.

Using curl: `curl -i  http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/group-by-color`.
For browser: [http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/group-by-color](http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/group-by-color)

# Answer 6
Please check: `com.kowsercse.webservice.ProfileResource`. Spring mvc is used for webservice.

Using curl: `curl -i  http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/group-by-color-with-names`.
For browser: [http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/group-by-color-with-names](http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/group-by-color-with-names)

# Answer 7
Use following curl command to POST and create a new profile:
`curl -i -H "Content-Type: application/json" -X POST -d '{"firstName":"John","lastName":"Doe","address":"United States","zipCode":"99999","phoneNumber":"999-999-9999","color":"Yellow"}' http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile`


```
$ curl -i -H "Content-Type: application/json" -X POST -d '{"firstName":"John","lastName":"Doe","address":"United States","zipCode":"99999","phoneNumber":"999-999-9999","color":"Yellow"}' http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile
HTTP/1.1 201 Created
Server: Apache-Coyote/1.1
Location: http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/1
Content-Length: 0
Date: Sat, 02 Apr 2016 11:32:48 GMT
```

Location header returns the uri for profile resource created as shown `http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/6`.

```
$ curl -i http://ipeertest.cloudapp.net:8080/profile-webservice/webservice/profile/1
HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Content-Type: application/json
Content-Length: 134
Date: Sat, 02 Apr 2016 11:36:17 GMT

{"id":6,"firstName":"John","lastName":"Doe","address":"United States","zipCode":"99999","phoneNumber":"999-999-9999","color":"Yellow"}
```

I believe it answers the questions.
