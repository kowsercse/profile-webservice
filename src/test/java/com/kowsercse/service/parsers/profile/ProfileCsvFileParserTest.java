package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @author kowsercse@gmail.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring.xml"})
public class ProfileCsvFileParserTest {

  @Autowired
  private ProfileCsvFileParser profileCsvFileParser;

  @Test(expected = IllegalArgumentException.class)
  public void testNullFile() throws URISyntaxException, IOException {
    profileCsvFileParser.parseFile(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidFile() throws URISyntaxException, IOException {
    final File invalidFile = new File("/invalid_file");
    profileCsvFileParser.parseFile(invalidFile);
  }

  @Test
  public void testParseFile() throws URISyntaxException, IOException {
    final File testInputFile = new File(getClass().getResource("/profile.csv").toURI());
    final List<Profile> profiles = profileCsvFileParser.parseFile(testInputFile);
    Assert.assertEquals(5, profiles.size());
  }

}
