package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author kowsercse@gmail.com
 */
public class DefaultProfileParserTest {

  private DefaultProfileParser profileParser;

  @Before
  public void setUp() {
    profileParser = new DefaultProfileParser(
        "^%s, %s, %s, %s, %s$",
        Arrays.asList(FieldMapper.lastName, FieldMapper.firstName, FieldMapper.phoneNumber, FieldMapper.color, FieldMapper.zipCode)
    );
  }

  @Test(expected = NullPointerException.class)
  public void parseNullProfile() {
    profileParser.parseProfile(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void parseBlankProfile() {
    profileParser.parseProfile("");
  }

  @Test
  public void parseValidProfile() {
    final Profile profile = profileParser.parseProfile("Duck, Donald, (703)-742-0996, Golden, 99999");

    Assert.assertNotNull(profile);
    Assert.assertEquals("Donald", profile.getFirstName());
    Assert.assertEquals("Duck", profile.getLastName());
    Assert.assertEquals("Golden", profile.getColor());
    Assert.assertEquals("99999", profile.getZipCode());
    Assert.assertEquals("(703)-742-0996", profile.getPhoneNumber());
    Assert.assertNull(profile.getAddress());
  }

  @Test
  public void parseValidProfileWithExtendedZipCode() {
    final Profile profile = profileParser.parseProfile("Duck, Donald, (703)-742-0996, Golden, 99999-1234");

    Assert.assertNotNull(profile);
    Assert.assertEquals("Donald", profile.getFirstName());
    Assert.assertEquals("Duck", profile.getLastName());
    Assert.assertEquals("Golden", profile.getColor());
    Assert.assertEquals("99999-1234", profile.getZipCode());
    Assert.assertEquals("(703)-742-0996", profile.getPhoneNumber());
    Assert.assertNull(profile.getAddress());
  }


  @Test(expected = IllegalArgumentException.class)
  public void parseInvalidProfile() {
    profileParser.parseProfile("Donald Duck");
  }

  @Test(expected = IllegalArgumentException.class)
  public void parseValidProfileWithExtraCharacter() {
    profileParser.parseProfile("Duck, Donald, (703)-742-0996, Golden, 99999 ");
  }

}
