package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author kowsercse@gmail.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring.xml"})
public class ProfileCsvParserTest {

  @Autowired
  private ProfileCsvParser profileCsvParser;

  @Test
  public void parseFormat1() {
    final Profile profile = profileCsvParser.parseProfile("Duck, Donald, (703)-742-0996, Golden, 99999");

    Assert.assertNotNull(profile);
    Assert.assertEquals("Donald", profile.getFirstName());
    Assert.assertEquals("Duck", profile.getLastName());
    Assert.assertEquals("Golden", profile.getColor());
    Assert.assertEquals("99999", profile.getZipCode());
    Assert.assertEquals("(703)-742-0996", profile.getPhoneNumber());
    Assert.assertNull(profile.getAddress());
  }

  @Test
  public void parseFormat2() {
    final Profile profile = profileCsvParser.parseProfile("Donald Duck, Golden, 99999-1234, 703 955 0373");

    Assert.assertNotNull(profile);
    Assert.assertEquals("Donald", profile.getFirstName());
    Assert.assertEquals("Duck", profile.getLastName());
    Assert.assertEquals("Golden", profile.getColor());
    Assert.assertEquals("99999-1234", profile.getZipCode());
    Assert.assertEquals("703 955 0373", profile.getPhoneNumber());
    Assert.assertNull(profile.getAddress());
  }

  @Test
  public void parseFormat3() {
    final Profile profile = profileCsvParser.parseProfile("Donald, Duck, 99999, 646 111 0101, Golden");

    Assert.assertNotNull(profile);
    Assert.assertEquals("Donald", profile.getFirstName());
    Assert.assertEquals("Duck", profile.getLastName());
    Assert.assertEquals("Golden", profile.getColor());
    Assert.assertEquals("99999", profile.getZipCode());
    Assert.assertEquals("646 111 0101", profile.getPhoneNumber());
    Assert.assertNull(profile.getAddress());
  }

  @Test
  public void parseFormat4() {
    final Profile profile = profileCsvParser.parseProfile("Donald Duck, 1 Disneyland, 99999, 876-543-2104, Golden");

    Assert.assertNotNull(profile);
    Assert.assertEquals("Donald", profile.getFirstName());
    Assert.assertEquals("Duck", profile.getLastName());
    Assert.assertEquals("Golden", profile.getColor());
    Assert.assertEquals("99999", profile.getZipCode());
    Assert.assertEquals("876-543-2104", profile.getPhoneNumber());
    Assert.assertEquals("1 Disneyland", profile.getAddress());
  }

  @Test(expected = NullPointerException.class)
  public void parseNull() {
    profileCsvParser.parseProfile(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void parseBlank() {
    profileCsvParser.parseProfile("");
  }


  @Test(expected = IllegalArgumentException.class)
  public void parseInvalidProfile() {
    profileCsvParser.parseProfile("Donald Duck");
  }

  @Test(expected = IllegalArgumentException.class)
  public void parseValidProfileWithExtraCharacter() {
    profileCsvParser.parseProfile("Duck, Donald, (703)-742-0996, Golden, 99999 ");
  }

}
