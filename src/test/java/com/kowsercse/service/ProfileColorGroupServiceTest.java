package com.kowsercse.service;

import com.kowsercse.entity.Profile;
import com.kowsercse.service.parsers.profile.ProfileCsvFileParser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @author kowsercse@gmail.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring.xml"})
public class ProfileColorGroupServiceTest {

  @Autowired
  private ProfileColorGroupService profileColorGroupService;

  @Autowired
  private ProfileCsvFileParser profileCsvFileParser;

  @Test
  public void testGroupByColor() throws Exception {
    final File testInputFile = new File(getClass().getResource("/valid-profiles.csv").toURI());
    final List<Profile> profiles = profileCsvFileParser.parseFile(testInputFile);
    final Map<String, List<Profile>> byColor = profileColorGroupService.groupByColor(profiles);

    Assert.assertEquals(3, byColor.get("Golden").size());
    Assert.assertEquals(2, byColor.get("Red").size());
  }

  @Test
  public void printColorGroupCount() throws URISyntaxException, IOException {
    final File testInputFile = new File(getClass().getResource("/valid-profiles.csv").toURI());
    final List<Profile> profiles = profileCsvFileParser.parseFile(testInputFile);
    profileColorGroupService.printColorGroupCount(profiles);
  }

  @Test
  public void printColorGroupWithNames() throws Exception {
    final File testInputFile = new File(getClass().getResource("/valid-profiles.csv").toURI());
    final List<Profile> profiles = profileCsvFileParser.parseFile(testInputFile);
    profileColorGroupService.printColorGroupCountWithNames(profiles);
  }
}
