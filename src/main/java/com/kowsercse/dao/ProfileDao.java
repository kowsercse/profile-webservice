package com.kowsercse.dao;

import com.kowsercse.entity.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author kowsercse@gmail.com
 */
@Repository
public class ProfileDao {

  @PersistenceContext
  private EntityManager entityManager;

  @Transactional(Transactional.TxType.REQUIRED)
  public void add(final Profile profile) {
    entityManager.persist(profile);
  }

  public List<Profile> findAll() {
    final TypedQuery<Profile> query = entityManager.createQuery("SELECT p FROM Profile AS p", Profile.class);
    return query.getResultList();
  }

  public Profile findById(final int profileId) {
    return entityManager.find(Profile.class, profileId);
  }
}
