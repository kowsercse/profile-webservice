package com.kowsercse.entity;

import javax.persistence.*;

/**
 * @author kowsercse@gmail.com
 */
@Entity
@Table(name = "profile")
public class Profile {

  private Integer id;
  private String firstName;
  private String lastName;
  private String address;
  private String zipCode;
  private String phoneNumber;
  private String color;

  @Id
  @GeneratedValue
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "first_name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  @Basic
  @Column(name = "last_name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  @Basic
  @Column(name = "address")
  public String getAddress() {
    return address;
  }

  public void setAddress(final String address) {
    this.address = address;
  }

  @Basic
  @Column(name = "zip_code")
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(final String zipCode) {
    this.zipCode = zipCode;
  }

  @Basic
  @Column(name = "phone_number")
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Basic
  @Column(name = "color")
  public String getColor() {
    return color;
  }

  public void setColor(final String color) {
    this.color = color;
  }

  @Override
  public String toString() {
    return firstName + " " + lastName;
  }
}
