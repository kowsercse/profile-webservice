package com.kowsercse.service;

import com.kowsercse.entity.Profile;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kowsercse@gmail.com
 */
@Service
public class ProfileColorGroupService {

  /**
   * Group profiles by color.
   * @param profiles The profiles to be grouped
   * @return Map where key is the color and value is list of profiles for this color
   */
  public Map<String, List<Profile>> groupByColor(final List<Profile> profiles) {
    Map<String, List<Profile>> coloredProfilesMap = new HashMap<>();
    for (final Profile profile : profiles) {
      coloredProfilesMap.compute(profile.getColor(), (color, values) -> {
        if (values == null) {
          values = new ArrayList<>();
        }

        values.add(profile);
        return values;
      });
    }

    return coloredProfilesMap;
  }

  public void printColorGroupCount(final List<Profile> profiles) {
    final Map<String, List<Profile>> groupByColor = groupByColor(profiles);
    groupByColor.forEach((color, coloredProfiles) -> System.out.println(color + " " + coloredProfiles.size()));
  }

  public void printColorGroupCountWithNames(final List<Profile> profiles) {
    final Map<String, List<Profile>> groupByColor = groupByColor(profiles);
    groupByColor.forEach((color, coloredProfiles) -> {
      final String names = StringUtils.join(coloredProfiles, ", ");
      System.out.println(color + " " + coloredProfiles.size() + " " + names);
    });
  }

}
