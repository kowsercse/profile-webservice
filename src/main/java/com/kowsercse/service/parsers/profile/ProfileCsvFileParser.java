package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kowsercse@gmail.com
 */
@Service
/**
 * The parser to read profile information from a CSV file. This parser accepts a file with CSV data and returns List
 * of Profile for each valid line in the CSV file.
 */
public class ProfileCsvFileParser {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final ProfileCsvParser profileCsvParser;

  @Autowired
  public ProfileCsvFileParser(final ProfileCsvParser profileCsvParser) {
    this.profileCsvParser = profileCsvParser;
  }

  /**
   * The parser to read profile information from a CSV file. This parser accepts a file with CSV data and returns List
   * of Profile for each valid line in the CSV file. It silently ignores invalid line and print warning in the log file.
   *
   * @param file The file contains CSV data for Profile.
   * @return List of profiles which are successfully parsed.
   *
   * @throws IOException              If any I/O error occurs.
   * @throws IllegalArgumentException If the file is null or does not exist.
   */
  public List<Profile> parseFile(final File file) throws IOException {
    if (file == null || !file.isFile()) {
      throw new IllegalArgumentException("File does not exists or null: " + file);
    }

    final List<Profile> profiles = new ArrayList<>();
    final LineIterator lineIterator = FileUtils.lineIterator(file);

    int lineNumber = 0;
    while (lineIterator.hasNext()) {
      final String line = lineIterator.next();
      lineNumber++;
      try {
        final Profile profile = profileCsvParser.parseProfile(line);
        profiles.add(profile);
      }
      catch (IllegalArgumentException | NullPointerException ex) {
        logger.warn("Failed to parse line number: " + lineNumber + " for data: [" + line + "]", ex);
      }
    }

    return profiles;
  }

}
