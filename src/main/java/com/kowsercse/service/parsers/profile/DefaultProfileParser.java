package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;
import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kowsercse@gmail.com
 */
public class DefaultProfileParser implements ProfileParser {

  private final Pattern pattern;
  private final List<FieldMapper> fieldMappers;

  /**
   * @param format       The format for profile
   * @param fieldMappers FieldMappers according to their sequence in the format parameter
   * @throws IllegalArgumentException If fieldMappers mismatches with format
   */
  public DefaultProfileParser(final String format, final List<FieldMapper> fieldMappers) {
    final String regex = createRegularExpression(format, fieldMappers);

    this.pattern = Pattern.compile(regex);
    this.fieldMappers = fieldMappers;
  }

  @Override
  public boolean supports(final String profileData) {
    final Matcher matcher = pattern.matcher(profileData);
    return matcher.find();
  }

  @Override
  public Profile parseProfile(final String profileData) {
    Validate.notBlank(profileData, "profileData should not be null or empty");
    final Matcher matcher = pattern.matcher(profileData);
    final boolean success = matcher.find();
    if (success) {
      final Profile profile = new Profile();
      fieldMappers.forEach(fieldMapper -> fieldMapper.mapProfile(profile, matcher));

      return profile;
    }

    throw new IllegalArgumentException("Profile Data does not matches given pattern");
  }

  private String createRegularExpression(final String format, final List<FieldMapper> fieldMappers) {
    final String[] args = new String[fieldMappers.size()];
    for (int i = 0; i < fieldMappers.size(); i++) {
      args[i] = fieldMappers.get(i).getPattern();
    }
    return String.format(format, args);
  }

}
