package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;

import java.util.function.BiConsumer;
import java.util.regex.Matcher;

/**
 * The enum to map field for Profile. It takes the regex pattern and able to set the corresponding profile field.
 *
 * FieldMapper.firstName.mapProfile(profile, matcher) read firstName from the mather and set the profile.firstName
 *
 * @author kowsercse@gmail.com
 */
public enum FieldMapper {
  firstName("\\w+", Profile::setFirstName),
  lastName("\\w+", Profile::setLastName),
  phoneNumber("(\\(\\d{3}\\)\\-\\d{3}\\-\\d{4})|(\\d{3} \\d{3} \\d{4})|(\\d{3}-\\d{3}-\\d{4})", Profile::setPhoneNumber),
  color("\\w+", Profile::setColor),
  zipCode("((\\d{5})|(\\d{5}-\\d{4}))", Profile::setZipCode),
  address("[\\p{Alnum}\\s]+", Profile::setAddress);

  private final String pattern;
  private final BiConsumer<Profile, String> mapper;

  FieldMapper(final String pattern, final BiConsumer<Profile, String> mapper) {
    this.mapper = mapper;
    this.pattern = String.format("(?<%s>%s)", name(), pattern);
  }

  /**
   * @return The regex pattern for the field
   */
  public String getPattern() {
    return pattern;
  }

  /**
   * Maps the group from mathcer to the corresponding profile field.
   *
   * FieldMapper.firstName.mapProfile(profile, matcher) read firstName from the mather and set the profile.firstName
   *
   * @param profile The profile to set the field
   * @param matcher Matcher is read to get the value for profile's field
   */
  public void mapProfile(final Profile profile, final Matcher matcher) {
    mapper.accept(profile, matcher.group(name()));
  }

}
