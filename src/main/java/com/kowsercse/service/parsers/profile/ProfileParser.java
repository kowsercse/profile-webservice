package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;

/**
 * The parser to parse a Profile from a string data
 * @author kowsercse@gmail.com
 */
public interface ProfileParser {

  /**
   * @param profileData Check if the profileData is parsable using this parser
   * @return boolean Returns true if profileData is parsable using this parser
   */
  boolean supports(String profileData);

  /**
   * @param profileData The profile data to be parsed
   * @return Profile The parsed profile if parsing is successful
   *
   * @throws IllegalArgumentException If profile data is unsupported by the parser
   * @throws NullPointerException If profile data is null
   */
  Profile parseProfile(final String profileData);

}
