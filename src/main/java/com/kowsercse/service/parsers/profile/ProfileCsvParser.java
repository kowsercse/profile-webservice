package com.kowsercse.service.parsers.profile;

import com.kowsercse.entity.Profile;

import java.util.LinkedList;
import java.util.List;

/**
 * @author kowsercse@gmail.com
 */
public class ProfileCsvParser {

  private List<ProfileParser> profileParsers = new LinkedList<>();

  public void setProfileParsers(final List<ProfileParser> profileParsers) {
    this.profileParsers = profileParsers;
  }

  /**
   * @param profileData The profile data to be parsed
   * @return Profile The parsed profile if parsing is successful
   *
   * @throws IllegalArgumentException If profile data is unsupported by the parser
   * @throws NullPointerException If profile data is null
   */
  public Profile parseProfile(final String profileData) {
    final ProfileParser profileParser = getSupportedProfileParser(profileData);
    if (profileParser != null) {
      return profileParser.parseProfile(profileData);
    }

    throw new IllegalArgumentException("Profile data format is not supported");
  }

  private ProfileParser getSupportedProfileParser(final String profileData) {
    for (final ProfileParser profileParser : profileParsers) {
      if (profileParser.supports(profileData)) {
        return profileParser;
      }
    }

    return null;
  }

}
