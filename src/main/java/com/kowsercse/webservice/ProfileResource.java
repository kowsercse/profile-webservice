package com.kowsercse.webservice;

import com.kowsercse.dao.ProfileDao;
import com.kowsercse.entity.Profile;
import com.kowsercse.service.ProfileColorGroupService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author kowsercse@gmail.com
 */
@Controller
@RequestMapping("/webservice/profile")
public class ProfileResource {

  @Autowired
  private ProfileDao profileDao;
  @Autowired
  private ProfileColorGroupService profileColorGroupService;

  @RequestMapping(value = "/group-by-color", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity getGroupByColor() {
    final Map<String, List<Profile>> profilesByColor = getProfilesByColor();

    final List<ImmutablePair<String, Integer>> values = new ArrayList<>();
    profilesByColor.forEach((color, profiles) -> values.add(ImmutablePair.of(color, profiles.size())));

    return ResponseEntity.ok(values);
  }

  @RequestMapping(value = "/group-by-color-with-names", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity getGroupByColorWithNames() {
    final Map<String, List<Profile>> profilesByColor = getProfilesByColor();

    final List<ImmutableTriple<String, Integer, List<String>>> values = new ArrayList<>();
    profilesByColor.forEach((color, profiles) -> {
      final List<String> names = profiles.stream()
          .map(profile -> String.format("%s %s", profile.getFirstName(), profile.getLastName()))
          .collect(Collectors.toList());
      values.add(ImmutableTriple.of(color, profiles.size(), names));
    });

    return ResponseEntity.ok(values);
  }

  private Map<String, List<Profile>> getProfilesByColor() {
    final List<Profile> allProfiles = profileDao.findAll();
    return profileColorGroupService.groupByColor(allProfiles);
  }

  @RequestMapping(value = "/{profileId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity get(@PathVariable("profileId") final int profileId) {
    final Profile profile = profileDao.findById(profileId);
    if (profile != null) {
      return ResponseEntity.ok(profile);
    }

    return ResponseEntity.notFound().build();
  }

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity post(@RequestBody final Profile profile) {
    profileDao.add(profile);
    final UriComponents uriComponents = ServletUriComponentsBuilder.fromCurrentContextPath()
        .pathSegment("webservice", "profile", profile.getId().toString())
        .build();
    return ResponseEntity.created(uriComponents.toUri()).build();
  }

}
