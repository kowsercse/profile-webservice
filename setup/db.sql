CREATE TABLE `profile` (
  id           INT(11)      NOT NULL AUTO_INCREMENT,
  first_name   VARCHAR(256) NOT NULL,
  last_name    VARCHAR(256),
  address      VARCHAR(512) NOT NULL,
  zip_code     VARCHAR(16) NOT NULL,
  phone_number VARCHAR(32)  NOT NULL,
  color        VARCHAR(32)  NOT NULL,
  PRIMARY KEY (id)
)
